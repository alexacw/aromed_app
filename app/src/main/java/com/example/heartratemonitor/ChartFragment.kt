package com.example.heartratemonitor

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResultCallback
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.heartratemonitor.databinding.FragmentChartBinding
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.Legend.LegendForm
import com.github.mikephil.charting.components.YAxis.AxisDependency
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.utils.ColorTemplate
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.time.Instant


/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class ChartFragment : Fragment() {

    private lateinit var binding: FragmentChartBinding
    private lateinit var chart: LineChart
    private val initTime = Instant.now()
    private lateinit var openFileResultLauncher: ActivityResultLauncher<Intent>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        openFileResultLauncher = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult(),
            ActivityResultCallback {
                if (it.resultCode == AppCompatActivity.RESULT_OK) {
                    // There are no request codes
                    this.saveUri = it.data?.data
                    Log.i("createFile", "created file @ ${this.saveUri}")
                    this.binding.buttonFirst.text = "Save to ${this.saveUri?.encodedPath}"
                }

            }
        )

        binding = FragmentChartBinding.inflate(inflater, container, false)
        chart = binding.chart
        chart.description.isEnabled = true
        chart.setTouchEnabled(true)
        chart.isDragEnabled = true
        chart.setScaleEnabled(true)
        chart.setDrawGridBackground(false)
        chart.setPinchZoom(true)
        chart.setBackgroundColor(Color.BLACK)
        chart.setVisibleXRangeMaximum(10000F)
        val data = LineData()
        data.setValueTextColor(Color.WHITE)
        chart.data = data
        val l = chart.legend
        l.form = LegendForm.LINE
        l.textColor = Color.WHITE
        val xl = chart.xAxis
        xl.textColor = Color.WHITE
        xl.setDrawGridLines(false)
        xl.setAvoidFirstLastClipping(true)
        xl.isEnabled = true
        val leftAxis = chart.axisLeft
        leftAxis.textColor = Color.WHITE
        leftAxis.axisMaximum = Y_RANGE
        leftAxis.axisMinimum = -Y_RANGE
        leftAxis.setDrawGridLines(true)
        val rightAxis = chart.axisRight
        rightAxis.textColor = Color.WHITE
        rightAxis.axisMaximum = Y_RANGE
        rightAxis.axisMinimum = -Y_RANGE
        rightAxis.setDrawGridLines(true)
        rightAxis.isEnabled = true

        binding.buttonFirst.setOnClickListener {
            writeToCsv()
        }

        return binding.root
    }

    class LPF(var v: Float, private val w: Float) {
        fun update(newV: Float): Float {
            this.v = (1f - w) * this.v + w * newV
            return this.v
        }
    }

    private val tsLpfMap = mutableMapOf<String, LPF>()

    fun addEntry(label: String, v: Float, t: Instant) {
        val data: LineData = chart.data
        val vFiltered = tsLpfMap.getOrPut(label) { LPF(v, 0.01f) }.update(v)

        val ds = data.getDataSetByLabel(label, true) ?: data.addDataSet(createSet(label))
            .let { data.getDataSetByLabel(label, true)!! }
        val x = (t.toEpochMilli() - initTime.toEpochMilli()).toFloat()
        ds.addEntry(
            Entry(
                x,
                v - vFiltered
            )
        )
        Log.i(label, (v - vFiltered).toString())
        if (x > 10000 && ds.entryCount > 10000)
            ds.removeFirst()
        data.notifyDataChanged()
        chart.notifyDataSetChanged()
        chart.setVisibleXRangeMaximum(5000F)
        chart.moveViewToX(x)

    }

    private var latestColorIndex = 0
    private fun createSet(label: String): LineDataSet {
        latestColorIndex++
        val set = LineDataSet(null, label)
        set.axisDependency =
            AxisDependency.values()[label.hashCode() % AxisDependency.values().size]
        set.setDrawCircles(false)
        set.color =
            ColorTemplate.MATERIAL_COLORS[latestColorIndex % ColorTemplate.MATERIAL_COLORS.size]
        set.setCircleColor(Color.WHITE)
        set.lineWidth = 2f
        set.fillAlpha = 65
        set.fillColor =
            ColorTemplate.MATERIAL_COLORS[latestColorIndex % ColorTemplate.MATERIAL_COLORS.size]
        set.highLightColor = Color.rgb(244, 117, 117)
        set.valueTextColor = Color.WHITE
        set.valueTextSize = 9f
        set.setDrawValues(false)
        return set
    }

    private fun writeToCsv() {
        if (this.saveUri != null) {
            try {
                val contentResolver = requireContext().contentResolver

                contentResolver.openFileDescriptor(this.saveUri!!, "w")
                    ?.use { parcelFileDescriptor ->
                        FileOutputStream(parcelFileDescriptor.fileDescriptor).use { fileOutputStream ->
                            fileOutputStream.write(
                                ("Overwritten at ${System.currentTimeMillis()}\n")
                                    .toByteArray()
                            )
                        }
                    }
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        } else {
            pickOutputFile()
        }

    }

    private var saveUri: Uri? = null

    private fun pickOutputFile() {

        val intent = Intent(Intent.ACTION_CREATE_DOCUMENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = "application/csv"
            putExtra(Intent.EXTRA_TITLE, "invoice.csv")
        }
        openFileResultLauncher.launch(intent)
    }

    companion object {
        const val Y_RANGE: Float = 50000f
    }
}